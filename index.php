<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         
<link rel="stylesheet" type="text/css" href="style.css" >
        <script src="localStorage.js" defer></script>

        <title>web3</title>
    </head>
    <body>
    <div class="main-form">
       <br>
       <br>
                <form  action="form.php" method="post">
                   
                        <label><strong>Name</strong></label>
                        <input type="text" class="form-control" name="Name" placeholder=""><br>
                    
                  
                        <label><strong>Email</strong></label>
                        <input type="text" class="form-control" name="E_mail" placeholder=""><br>
                   
                   
                        <label><strong>Date</strong></label>
                        <select " name="Date">
                         <option value="1998">2007</option>
                         <option value="1998">2006</option>
                         <option value="1998">2005</option>
                         <option value="1998">2004</option>
                         <option value="1998">2003</option>
                            <option value="2002">2002</option>
                            <option value="2001">2001</option>
                            <option value="2000">2000</option>
                            <option value="1999">1999</option>
                            <option value="1998">1998</option>
                              <option value="1998">1997</option>
                                <option value="1998">1996</option>
                        </select>
                     <br>
                   
                        <label><strong>Gender</strong></label><br>
                     
                            <input type="radio" name="Gender" value="Male" checked>
                            <label>
                                Male
                            </label>
                        
                        <div>
                        
                            <input type="radio" name="Gender" value="Female">
                            <label>
                                Female
                            </label>
                        </div>
                     
                     	<div >
                        <label><strong>Number of limbs</strong></label><br>
                        <input type="radio" name="Number_of_limbs" value="1" checked="checked">
                        <label>1</label>
                        <input type="radio" name="Number_of_limbs" value="2">
                        <label>2</label>
                        <input type="radio" name="Number_of_limbs" value="3">
                        <label>3</label>
                        <input type="radio" name="Number_of_limbs" value="4">
                        <label>4</label>
                        <input type="radio" name="Number_of_limbs" value="8">
                        <label>8</label>
                    </div>
                    <div class="form-group">
                        <label><strong>Superpowers</strong></label><br>
                        <select multiple name="Superpowers" >
                          <option value="Immortality" selected="selected">Immortality</option>
                          <option value="Passing through walls">Passing through walls</option>
                          <option value="Levitation">Levitation</option>
                            <option value="Mind reading">Mind reading</option>
                              <option value="Teleportation">Teleportation</option>
                            
                        </select>
                    </div>
                     <br>
                    <div >
                        <label><strong>Biografia</strong></label><br>
                        <textarea name="Biografia"  placeholder=" text"></textarea>
                    </div>
                    <div >
                        <input type="checkbox" name="Accept">
                        <label>
                            Accept the privacy policy
                        </label>
                    </div>
                     <br>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div> 
    </body>
</html>